import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Login';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text(_title),
        ),
        body: LayoutBuilder(builder: (context, dimensions) {
          final width = dimensions.maxWidth / 1.3;
          final height = dimensions.maxHeight / 1.5;
          return Center(
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(maxWidth: width, minHeight: height),
                child: const LoginForm(),
              ),
            ),
          );
        }),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _key = GlobalKey<FormState>();
  final cuilController = TextEditingController();
  final usuarioController = TextEditingController();
  final claveController = TextEditingController();

  @override
  void dispose() {
    cuilController.dispose();
    usuarioController.dispose();
    claveController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String? _validarCUIT(String? value) {
      if (value!.isEmpty) {
        return "El campo cuit/cuil no puede estar vacio";
      } else if(value.length < 11) {
        return "El campo cuit/cuil debe tener 11 caracteres";
      } else {
        return null;
      }
    }

    String? _validarUsuario(String? value) {
      if (value!.isEmpty) {
        return "El campo usuario no puede estar vacio";
      } else {
        return null;
      }
    }

    String? _validarClave(String? value) {
      if (value!.isEmpty) {
        return "El campo clave no puede estar vacio";
      } else {
        return null;
      }
    }

    void _acceder() {
      if (_key.currentState!.validate()) {

        const int cuilBase = 20385429801;
        const String usuarioBase = "tegzov";
        const String claveBase = "miclave";

        final cuil = int.tryParse(cuilController.text);
        final usuario = usuarioController.text;
        final clave = claveController.text;

        if (cuil == cuilBase && usuario == usuarioBase && clave == claveBase) {
          print("Bienvenido, USUARIO: $usuario de CUIL $cuil y CLAVE $clave");
        } else {
          print("Sus credenciales no coinciden");
        }
      } else {
        print("Error en los campos!");
      }
    }

    return Form(
      key: _key,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          TextFormField(
            decoration: const InputDecoration(
              icon: Icon(Icons.badge_outlined),
              hintText: "CUIT/CUIL",
            ),
            validator: _validarCUIT,
            controller: cuilController,
            maxLength: 11,
            keyboardType: TextInputType.number,
          ),
          TextFormField(
            decoration: const InputDecoration(
              icon: Icon(Icons.person),
              hintText: "Usuario",
            ),
            validator: _validarUsuario, //_validarUsuario,
            controller: usuarioController,
          ),
          TextFormField(
            decoration: const InputDecoration(
              icon: Icon(Icons.vpn_key),
            ),
            validator: _validarClave,
            controller: claveController,
            obscureText: true,
          ),
          ElevatedButton(onPressed: _acceder, child: const Text("Acceder"))
        ],
      ),
    );
  }
}
